#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 17:59:29 2018

@author: Shainy
"""

import mnist_loader
import random
import numpy as np

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

def sigma(z):
    return 1.0/(1.0 + np.exp(-z))

def sigma_prime(z):
    return sigma(z)*(1 - sigma(z))

def initialize_variables(list_of_layer_sizes):
    number_of_layers = len(list_of_layer_sizes)
    weights = [np.random.randn(j, i+1) for i, j in zip(list_of_layer_sizes[:-1], list_of_layer_sizes[1:])]
    return number_of_layers, weights

def network_output(inp, weights):
    inputs = []
    nets = []
    inp = np.append(inp, [[-1.]], axis=0)
    inputs.append(inp)
    for weight in weights:
        net = np.dot(weight, inp)
        nets.append(net)
        out = sigma(net)
        inp = np.append(out, [[-1.]], axis=0)
        inputs.append(inp)
    return out, inputs[:-1], nets

def backpropagation(out, inputs, nets, weights, number_of_layers, expected_out, eta):
    b = expected_out - out
    delta = np.multiply(b, sigma_prime(nets[-1]))
    weights[-1] = weights[-1] + eta * np.dot(delta, inputs[-1].transpose())
    for i in range(2, number_of_layers):
        b = np.dot(weights[-i+1].transpose(), delta)
        b = b[:-1]
        delta = np.multiply(b, sigma_prime(nets[-i]))
        weights[-i] = weights[-i] + eta * np.dot(delta, inputs[-i].transpose())
    return weights

def test(weights, test_data):
    test_result = [(np.argmax(network_output(x,weights)[0]), y) for (x, y) in test_data]
    return sum(int(x == y) for (x, y) in test_result)

def neural_network(list_of_layer_sizes, training_data, eta, test_data=None):
    number_of_layers, weights = initialize_variables(list_of_layer_sizes)
    for x, y in training_data:
        out, inputs, nets = network_output(x, weights)
        weights = backpropagation(out, inputs, nets, weights, number_of_layers, y, eta)
    if test_data:
        n_test = len(test_data)
        performance = test(weights, test_data)
        print "{0} out of {1} evaluated correctly".format(performance, n_test)
    else:
        print "Finished learning"
    return performance

neural_network([784, 30, 10], training_data, 0.1, test_data)