import numpy as np
import random as rnd
import mnist_loader
import matplotlib.pyplot as plt

def sigma(z):
    return 1.0/(1.0 + np.exp(-z))
def sigma_prime(z):
    return sigma(z)*(1 - sigma(z))

def initialize_variables(list_of_layer_sizes):
    number_of_layers = len(list_of_layer_sizes)
    weights = [np.random.randn(j, i+1) for i, j in zip(list_of_layer_sizes[:-1], list_of_layer_sizes[1:])]
    return number_of_layers, weights

def network_output(inp, weights):
    inputs = []
    nets = []
    inp = np.append(inp, [[-1.]], axis=0)
    inputs.append(inp)
    for weight in weights:
        net = np.dot(weight, inp)
        nets.append(net)
        out = sigma(net)
        inp = np.append(out, [[-1.]], axis=0)
        inputs.append(inp)
    return out, inputs[:-1], nets

def backpropagation(out, inputs, nets, weights, number_of_layers, expected_out, eta, batch_size):
    b = expected_out - out
    delta = np.multiply(b, sigma_prime(nets[-1]))
    weights[-1] = weights[-1] + eta * np.dot(delta, inputs[-1].transpose())
    for i in range(2, number_of_layers):
        b = np.dot(weights[-i+1].transpose(), delta)
        b = b[:-1]
        delta = np.multiply(b, sigma_prime(nets[-i]))
        weights[-i] = weights[-i] + eta/batch_size * np.dot(delta, inputs[-i].transpose())
    return weights

def test(weights, test_data):
    test_result = [(np.argmax(network_output(x,weights)[0]), y) for (x, y) in test_data]
    return sum(int(x == y) for (x, y) in test_result)

def neural_network(list_of_layer_sizes, training_data, eta, batch_size,epochs,test_data=None):
    print('\n eta {}'.format(eta))
    number_of_layers, weights = initialize_variables(list_of_layer_sizes)
    training_data = list(training_data)
    ep_results = np.zeros(epochs)
    for ep in range(0,epochs):
        rnd.shuffle(training_data)
        batches = [training_data[k:k+batch_size] for k in range(0,len(training_data),batch_size)]
        for batch in batches:
            for x, y in batch:
                out, inputs, nets = network_output(x, weights)
                weights = backpropagation(out, inputs, nets, weights, number_of_layers, y, eta, batch_size)
        if test_data:
            test_data = list(test_data)
            n_test = len(test_data)
            test_result = test(weights, test_data)
            print ("Epoch {} : {} / {}".format(ep+1,test_result, n_test))
            ep_results[ep] = test_result/n_test
        else:
            print ("Finished learning")
    #plt.plot(np.arange(1,31,1),ep_results)
    #plt.xlabel('epochs')
    #plt.ylabel('performance of the network')
    return ep_results

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
res = neural_network([784, 30, 10], training_data, 0.4, 100, 30, test_data)
'''res = []
eta = np.arange(0.1,1.1,0.1)
for m in range(0,len(eta)):
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
    res.append(neural_network([784, 30, 10], training_data, eta[m], 10, 30, test_data))
'''
"""x=np.arange(1,31,1)
i=0
for r in res:
    plt.plot(x,r*1000, label = '$\eta = {}$'.format(round(eta[i],1)))
    i += 1
plt.xticks(np.arange(0,32,2))
plt.xlabel('epoch')
plt.ylabel('performance of the network')
plt.legend(loc='upper left', bbox_to_anchor=(0.25, 0.37),
          fancybox=True, shadow=True, ncol=3)

ar=np.asarray(res)
np.savetxt('ep30_neuron30_batch10',ar)"""

"""
eta = np.arange(0.1,1.1,0.1)

for i in range(5,10):
    plt.plot(x,ar[i], label = '$\eta = {}$'.format(round(eta[i],1)))
plt.xticks(np.arange(0,32,2))
plt.xlabel('epoch')
plt.ylabel('performance of the network')
plt.legend()"""
ar=np.loadtxt('ep30_neuron30_batch10')
x=np.arange(1,31,1)
res15=res
plt.plot(x,ar[3], label = 'batch size = 10')
plt.plot(x,res15,label='batch size = 50')
plt.plot(x,res,label='batch size = 100')
plt.xticks(np.arange(0,32,2))
plt.xlabel('epoch')
plt.ylabel('performance of the network')
plt.legend()
